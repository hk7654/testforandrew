﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using InputManager.Consumables;

using Microsoft.VisualStudio.TestTools.UnitTesting;
using NUnit.Framework;

namespace TestProject
{
    [TestFixture]
    public class HamzaTests
    {
        private LoadDatabase _loadDatabase;

        [SetUp]
        public void Setup()
        {

            _loadDatabase = new LoadDatabase(new InputManagerClient(
                        new InputManagerClient.Settings()
                        {
                            ApiKey = "YXBpLWtleTp0ZXN0MTIzNA",
                            InputmanagerHostname = new Uri("https://zsqa-inputmanager-0027untagged2.qa.zsazure.com"),
                            IgnoreCertErrors = true, // DO NOT ENABLE THIS ON PROD,
                            HttpTimeout = TimeSpan.FromSeconds(15),
                            JobTimeout = TimeSpan.FromHours(3),
                            MaxRetryAttempts = 3
                        },
                        () => new StubTenantManager().TenantName), () => new SqlConnection(ConfigurationManager.ConnectionStrings["DBConnectionString"].ConnectionString));
        }

        [Test]
        public void RandomTest()
        {
            _loadDatabase.GetDataSql();
        }
    }
    public class LoadDatabase
    {
        private readonly IInputManagerClient _inputManagerClient;
        private readonly Func<IDbConnection> _connFactory;

        public LoadDatabase(IInputManagerClient inputManagerClient, Func<IDbConnection> connFactory)
        {
            _inputManagerClient = inputManagerClient;
            _connFactory = connFactory;
        }




        public void GetDataSql()
        {


            var metadata = _inputManagerClient.GetLatestMetadataAsync().Result;
            var timePeriods = _inputManagerClient.GetAllTimePeriods().Result;

        }



    }

    public class StubTenantManager
    {
        public string TenantName { get; set; }

        public StubTenantManager()
        {
            TenantName = "inputmanagerqauntagged2";
        }
    }
}
